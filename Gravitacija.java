import java.util.*;
import java.io.*;

class Gravitacija {
    
    public static double gravPospesek(double nadmVisina) {
        double g = 6.674*5.972;
        for(int i = 0; i < 13; i++)
            g *= 10;
        
        double totalVisina = 6.371;
        for(int i = 0; i < 6; i++)
            totalVisina *= 10;
        totalVisina += nadmVisina;
        
        g /= totalVisina*totalVisina;
        return g;
    }
    
    public static void izpis(PrintStream out, double nadmVisina, double gravPospesek) {
        out.printf("Nadmorska visina: %.2f%nGravitacijski pospesek: %.2f%n", nadmVisina, gravPospesek);
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double nadmVisina = sc.nextDouble();
        izpis(System.out, nadmVisina, gravPospesek(nadmVisina));
    }
}